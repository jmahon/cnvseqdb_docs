# cnvseqdb_docs

Miscellaneous documentation associated with CNVseq DB application

##### [Standard Operating Procedure (SOP)](CNVseqDB_SOP.pdf)
This copy is NOT for use diagnostically. Please use the version controlled copy on EQMS  


##### [Bioinformatics Protocols](binfx_seeding.md)
This covers initial installation, setup and seeding of data