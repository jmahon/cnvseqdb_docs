# CNVseq DB

This is a short supplimentary file to the main documentation available on LGL docs

## About CNVseq DB
This is a database to store CNVseq data and results, allowing them to be 
readily available, searchable and comparable. Runs and samples can be viewed, 
and imbalances can be scored.  

* [Installation](#installation)
* [Seeding](#seeding)
* [Database Management](#database-management)


### Dependencies
Python 3.6 or greater  
Apache Tika - detailed below  
Java - detailed below  
All other dependencies are provided in the repository `requirements.txt` file  
These can be installed by using `pin install -r requirements.txt`  

### Installation
Installation methods differ depending on whether installation is for local 
development or deployment. 

* [Installation (Local Development)](#installation-(local-development))
* [Installation (Deployment)](#installation-(deployment))

#### Installation (Local Development)
CNVseq DB uses PostgreSQL as a database.
Install psql
```bash
sudo apt update
sudo apt install postgresql postgresql-contrib
```
Log into psql using the postgres user
```bash
sudo -u postgres psql
```
Create the CNVseqDB database, user and set permissions. Choose a suitable password
```bash
CREATE DATABASE cnvseq_db;
CREATE USER cnvseq_db_user WITH PASSWORD 'cnvseq_db_password';
GRANT ALL PRIVILEGES ON DATABASE cnvseq_db TO cnvseq_db_user;
ALTER USER cnvseq_db_user CREATEDB;
\q
```
The project can be installed for local development by cloning this repository 
to a local directory
```bash
git clone https://gitlab.com/jmahon/cnvseq_db.git
```
Perform migrations and create super user
```bash
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser # Use the same username as the postgresql user
```
Run tests to ensure that the above has worked successfully
```bash
python manage.py test
```
When developing and testing locally, the inbuilt Django web server can be used 
to serve the application
```bash
python manage.py runserver
```

#### Installation (Deployment)
The project can be installed for remote deployment by cloning this repository 
to a directory. It is recommended that a specific application user is created,
and a project directory is created in this user's home directory. Full deployment 
instructions are detailed on the LGL docs site. Local development installation 
instrucitons are shown above.

### Java and Tika Dependencies
These are not required for testing, but are required for seeding data
#### Java
A local copy of Java is required.
Create a directory `dependencies/java`
```bash
# Download openJDK 13
wget https://download.java.net/java/GA/jdk13.0.2/d4173c853231432d94f001e99d882ca7/8/GPL/openjdk-13.0.2_linux-x64_bin.tar.gz
# Extract 
tar xvzf openjdk-13.0.2_linux-x64_bin.tar.gz
# Export the path
export PATH=~/dependencies/java/jdk-13.0.2/bin:$PATH
# Check export has worked
which java
```

#### Apache Tika
Create a directory `dependencies/tika`
```bash
# Download Tika server
wget https://www.apache.org/dyn/closer.cgi/tika/tika-server-1.23.jar
# Export the path
export TIKA_SERVER_JAR=~/dependencies/tika/tika-server-1.23.jar
```

### Seeding Data
Seeding is performed using Django management commands. 

It is performed in two steps.
- Collection of filepaths of each sample to be seeded
- Seeding of these filepaths into the database

seeding script can  This file can then be used to import 
all of the sample paths within into the database.  

##### File Path Collection
This part of seeding walks through a directory and collects the filepaths of 
all samples within, saving them to a file.
The files that are required when seeding are the HTML pipeline output file 
(e.g. `DATE_SAMPLEID.processed_PIPELINEINFORMATION_WORKSHEETID.html`) and PDF 
report for each patient (e.g. `CNVseq report SAMPLEID_PATIENTNAME.pdf`)  

A list of these file locations is used to seed the database. To obtain these 
file locations in the first instance, the seeding management command is run 
with the `-l --list` flag, with two following arguments of a directory to 
search and a file path to save the output list of file paths.

If a single sample should be seeded, path_to_data should be the path to that 
single sample. If a whole batch should be seeded, provide the path to the batch 
directory. 
```bash
python manage.py seeding -l “path_to_data” “path_to_list”
```
Once the command has completed, check the list file that has been created to 
verify that the required batches have been discovered correctly.


##### Data Insertion
Once the file paths have been collected, these samples can then be inserted 
into the database using the custom Django seeding command
Use the -i/--import flag to import these samples. If the samples should be 
imported as legacy samples, then add the -o/--old flag. Legacy samples will be 
imported as already scored and completed.Without this `-o` flag, only 
information from the pipeline will be imported so that the user can score 
samples within CNVseq DB.
```bash
python manage.py seeding -i “path_to_list” -o
```
Verify that the samples have been entered into the application successfully.


### Database Management
CNVseq DB uses PostgreSQL as a database management system. Installation is 
detailed above.

##### Wiping the Database
If the database needs to be wiped in the event of bad migrations or wishing
to reseed froms scratch, this can be done with a Django management command.  
```bash
python manage.py wipe_db`
```
This will prompt the user for confirmation before deleting all data that is 
present in the tables listed in the file
`cnvseq_db_app/management/commands/wipe_db.py`.

### Bad Migrations
If a bad migration has been performed and new migrations need to be made.
To allow migrations to be performed, delete all existing migrations from the 
`cnvseq_db_app/migrations/` directory (except `__init__.py`). Fresh migrations 
can then be performed using the `makemigrations` command.  
```bash
python manage.py makemigrations
```


### Testing
Run tests with  
`python manage.py test`

Getting test coverage  
```bash
coverage run --source='.' manage.py test cnvseq_db_app
coverage report -i
```